﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Primzahlen.utils;

/// <summary>
///     Utils class for Observable collections. This class provides extension methods to the ObservableCollection.
/// </summary>
public static class ObservableCollectionUtils
{
    /// <summary>
    ///     Adds a range of objects to the collection.
    /// </summary>
    /// <param name="collection">the Observable Collection</param>
    /// <param name="values">the values to be added</param>
    /// <typeparam name="T">the data type of the objects in the collection</typeparam>
    public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> values)
    {
        foreach (var value in values) collection.Add(value);
    }
}