﻿using System.Windows;
using Primzahlen.controller;

namespace Primzahlen;

/// <summary>
///     Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        DataContext = new PrimeController(MainCanvas);
    }
}