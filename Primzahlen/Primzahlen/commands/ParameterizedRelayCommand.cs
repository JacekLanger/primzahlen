﻿using System;
using System.Windows.Input;

namespace Primzahlen.commands;

public class ParameterizedRelayCommand<T> : ICommand
{
    private readonly Action<T> _action;

    public ParameterizedRelayCommand(Action<T> action)
    {
        _action = action;
    }

    public bool CanExecute(object? parameter)
    {
        return true;
    }

    public void Execute(object? parameter)
    {
        // null check before execution
        _action?.Invoke((T) parameter);
    }


    public event EventHandler? CanExecuteChanged;
}