﻿using System;
using System.Windows.Input;

namespace Primzahlen.commands;

/// <summary>
///     Relays an Action to as a button command.
/// </summary>
public class RelayCommand : ICommand
{
    private readonly Action _action;

    /// <summary>
    ///     Creates a new Relay command.
    /// </summary>
    /// <param name="action">the action associated with this command</param>
    public RelayCommand(Action action)
    {
        _action = action;
    }

    /// <summary>
    ///     Returns whether the action can be executed.
    /// </summary>
    /// <param name="parameter">optional parameters. this is not implemented</param>
    /// <returns>true by default</returns>
    public bool CanExecute(object? parameter)
    {
        return true;
    }

    /// <summary>
    ///     Executes the private action of this class.
    /// </summary>
    /// <param name="parameter">no parameters allowed at this point</param>
    public void Execute(object? parameter)
    {
        _action();
    }

    /// <summary>
    ///     Event to be fired on execution policy being changed.
    /// </summary>
    public event EventHandler? CanExecuteChanged;
}