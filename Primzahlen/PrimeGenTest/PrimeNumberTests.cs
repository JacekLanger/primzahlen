using NUnit.Framework;
using PrimeGen;

namespace PrimeGenTest;

public class PrimeNumberTests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void TestPrimeNumbers()
    {
        Assert.IsNotEmpty(PrimeGenerator.GeneratePrimesAsIntList(10));
        Assert.IsTrue(4 == PrimeGenerator.GeneratePrimesAsIntList(10).Count);
    }

    [Test]
    public void TestPrimeNumbersExactly()
    {
        var expected = new[] {2, 3, 5, 7, 11, 13, 17, 19};
        Assert.AreEqual(expected, PrimeGenerator.GeneratePrimesAsIntList(20));
    }

    [Test]
    public void TestIsPrimeNumbers()
    {
        var primes = new[] {2, 3, 5, 7, 11, 13, 17, 19};
        var notPrimes = new[] {4, 6, 8, 9, 10, 12, 14, 15, 16, 20};

        foreach (var prime in primes) Assert.IsTrue(PrimeGenerator.IsPrime(prime));
        foreach (var noPrime in notPrimes) Assert.IsFalse(PrimeGenerator.IsPrime(noPrime));
    }
}