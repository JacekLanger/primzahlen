﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using PrimeGen;
using Primzahlen.commands;
using Primzahlen.controller.etc;
using Primzahlen.utils;

namespace Primzahlen.controller;

/// <summary>
///     Prime number controller, responsible for the interaction between view and prime numbers.
/// </summary>
public class PrimeController : ControllerBase
{
    private readonly Canvas _canvas;
    private readonly ObservableCollection<bool> _primes;
    private int _currentStart;
    private int _maxHeight;
    private int _maxWidth;

    private ICommand _stopCommand;
    private CancellationTokenSource _tokenSource;
    private ICommand _updateCommand;

    /// <summary>
    ///     Creates a new Prime controller.
    /// </summary>
    /// <param name="canvas">the canvas to be used</param>
    public PrimeController(Canvas canvas)
    {
        _primes = new ObservableCollection<bool>();
        _primes.AddRange(PrimeGenerator.GeneratePrimeNumbers());
        _canvas = canvas;
        _currentStart = 0;
        MaxHeight = 100;
        MaxWidth = 100;
        DrawPrimeGrid();
    }

    public int MaxHeight
    {
        get => _maxHeight;
        set
        {
            _maxHeight = value;
            OnPropertyChanged();
        }
    }

    public int MaxWidth
    {
        get => _maxWidth;
        set
        {
            _maxWidth = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///     The Current Start Value.
    /// </summary>
    public int CurrentStart
    {
        get => _currentStart;
        set
        {
            _currentStart = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///     Invokes _updateCommand, if _updateCommand is null creates a new command and sets it to updateCommand.
    /// </summary>
    public ICommand UpdateCommand => _updateCommand ??= new ParameterizedRelayCommand<string>(val =>
    {
        int start;
        // invoke only if a valid integer value was provided
        var isValid = false;
        if (val.Length > 1)
            isValid = int.TryParse(val.TrimStart('0'), out start);
        else isValid = int.TryParse(val, out start);

        if (isValid) UpdatePrimes(start);
    });

    public ICommand StopCommand => _stopCommand ??= new RelayCommand(() => _tokenSource.Cancel());

    private void UpdatePrimes(int start)
    {
        _primes.Clear();
        _primes.AddRange(PrimeGenerator.GeneratePrimeNumbers(start, MaxHeight * MaxWidth));
        DrawPrimeGrid();
    }

    private void DrawPrimeGrid()
    {
        _canvas.Children.Clear();
        var width = 12;
        var height = 7.5;

        List<Polygon> polys = new();

        for (var h = 1; h <= MaxHeight; h++)
        for (var w = 1; w <= MaxWidth; w++)
        {
            var poly = new Polygon();
            poly.Points = new PointCollection
            {
                new(width * w, height * h),
                new(width * w, height * (h + 1)),
                new(width * (w + 1), height * (h + 1)),
                new(width * (w + 1), height * h)
            };

            poly.Stroke = Brushes.DarkGray;
            poly.StrokeThickness = 1;
            _canvas.Children.Add(poly);
            polys.Add(poly);
        }

        for (var i = 0; i < polys.Count; i++) polys[i].ToolTip = i + CurrentStart;

        for (var i = 0; i < _primes.Count; i++)
            if (_primes[i])
                polys[i].Fill = Brushes.Red;
    }
}