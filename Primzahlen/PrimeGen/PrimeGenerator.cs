﻿namespace PrimeGen;

public class PrimeGenerator
{
    /// <summary>
    ///     generates an bool array and sets all indices that are prime to true.
    /// </summary>
    /// <param name="start">the start value, defaults to 0</param>
    /// <param name="size">the size of the prime grid, defaults to 10_000</param>
    /// <returns>an array of bool</returns>
    public static bool[] GeneratePrimeNumbers(long start = 0, long size = 10_000)
    {
        var primes = new bool[size];

        for (var i = 2; i < primes.Length; i++) primes[i] = IsPrime(i + start);

        return primes;
    }

    /// <summary>
    ///     Checks if a given number is prime.
    /// </summary>
    /// <param name="number">the number to check</param>
    /// <returns>true if is prime</returns>
    public static bool IsPrime(long number)
    {
        // if number is 1 it is not prime and we can quit
        if (number == 1) return false;
        // we need only to check until the sqrt
        var sqrt = Math.Sqrt(number);
        for (var i = 2; i <= sqrt; i++)
            // if dividable by i it is not prime
            if (number % i == 0)
                return false;
        return true;
    }

    /// <summary>
    ///     Generates a list of prime numbers.
    /// </summary>
    /// <param name="cap">the highest number to check</param>
    /// <returns>List of primenumbers</returns>
    public static IList<long> GeneratePrimesAsIntList(long cap)
    {
        List<long> primes = new();
        List<long> notPrime = new();

        for (var i = 2; i < cap; i++)
            if (!notPrime.Contains(i))
            {
                var isPrime = true;
                foreach (var prime in primes)
                    if (i % prime == 0)
                    {
                        isPrime = false;
                        break;
                    }

                if (isPrime) primes.Add(i);

                // add all multiples of the prime number.
                for (var j = 1; j < cap / i; j++) notPrime.Add(i * j); // this will hold duplicates
            }

        return primes;
    }
}