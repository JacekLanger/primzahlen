﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Primzahlen.Annotations;

namespace Primzahlen.controller.etc;

/// <summary>
///     Baseclass for all controllers. This class implements the INotifyPropertyChanged interface.
/// </summary>
public class ControllerBase : INotifyPropertyChanged
{
    /// <summary>
    ///     Property changed event.
    /// </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>
    ///     Invokes the property changed event.
    /// </summary>
    /// <param name="propertyName">the name of the property that is being changed</param>
    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    ///     Sets a new value and invokes property changed.
    /// </summary>
    /// <param name="origin">the property</param>
    /// <param name="value">the new value</param>
    /// <typeparam name="T">the data type</typeparam>
    protected void SetProperty<T>(ref T origin, T value)
    {
        if (origin == null) throw new ArgumentNullException(nameof(origin));
        origin = value;
        OnPropertyChanged();
    }
}